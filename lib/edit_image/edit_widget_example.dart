import 'package:flutter/material.dart';

class EditWidgetExample extends StatefulWidget {
  const EditWidgetExample({super.key});

  @override
  State<EditWidgetExample> createState() => _EditWidgetExampleState();
}

typedef _AddWidgetBuilder = _AddWidgetAction Function();

class _EditWidgetExampleState extends State<EditWidgetExample> {
  static Map<_ActionName, _AddWidgetBuilder> get actionsMap => {};

  _ActionName action = _ActionName('none', Icons.close);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Widgets Example'),
      ),
      body: Column(
        children: [
          Expanded(
            child: Listener(
              onPointerDown: (event) {
                print('onPointerDown');
              },
              onPointerMove: (event) {},
              onPointerUp: (event) {},
              child: Container(),
            ),
          ),
          _buildOption(),
        ],
      ),
    );
  }

  Widget _buildOption() {
    return Column(
      children: [
        Row(
          children: [
            ...actionsMap.entries.map((e) => Expanded(child: _buildAction(e))),
          ],
        ),
      ],
    );
  }

  Widget _buildAction(MapEntry<_ActionName, _AddWidgetBuilder> e) {
    final tooltip = e.key.name;
    return RadioListTile<_ActionName>(
      value: e.key,
      groupValue: action,
      title: Text(tooltip),
      onChanged: (v) {
        setState(() {
          action = v!;
        });
      },
    );
  }
}

class _ActionName {
  final String name;
  final IconData icon;

  _ActionName(this.name, this.icon);
}

abstract class _AddWidgetAction {
  void onPointerDown(PointerDownEvent event);
  void onPointerMove(PointerMoveEvent event);
  void onPointerUp(PointerUpEvent event);
}
