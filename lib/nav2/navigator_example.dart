import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NavigatorExample extends StatefulWidget {
  const NavigatorExample({super.key});

  @override
  State<NavigatorExample> createState() => _NavigatorExampleState();
}

class _NavigatorExampleState extends State<NavigatorExample> {
  final _index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _Page(
        index: _index,
      ),
    );
  }
}

class MyPageRoute extends CupertinoPageRoute {
  final Widget child;

  MyPageRoute({
    required this.child,
  }) : super(builder: (context) => child);
}

class _Page extends StatelessWidget {
  const _Page({
    required this.index,
  });

  final int index;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page $index'),
      ),
      body: Center(
        child: Column(
          children: [
            Text('Page $index'),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MyPageRoute(
                    child: _Page(
                      index: index + 1,
                    ),
                  ),
                );
              },
              child: const Text('Next'),
            ),
          ],
        ),
      ),
    );
  }
}
