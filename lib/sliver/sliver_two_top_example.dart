import 'package:flutter/material.dart';

import 'sliver_header.dart';

/// 不好写, 不放在主页了, 运行下就知道原因了
///
/// 本意是为了写一个头部有3个Sliver, 固定1 3位置的, 结果不好写
class SliverTwoTopExample extends StatelessWidget {
  const SliverTwoTopExample({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverWrapper(
            builder: _buildFirstBar,
            maxHeight: 200,
            minHeight: 50,
            // floating: true,
            pinned: true,
          ),
          SliverWrapper(
            builder: _buildMiddleBar,
            maxHeight: 120,
            minHeight: 50,
          ),
          SliverWrapper(
            builder: _buildPinnedBar,
            maxHeight: 120,
            minHeight: 50 + 50.0,
            floating: true,
            pinned: true,
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              _buildItem,
              childCount: 20,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFirstBar(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: double.infinity,
      color: Colors.black.withOpacity(0.3),
      child: const Text('我是appbar'),
    );
  }

  Widget _buildMiddleBar(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: double.infinity,
      color: Colors.green.withOpacity(0.3),
      child: const Text('我是第二个'),
    );
  }

  Widget _buildPinnedBar(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    var top = shrinkOffset;
    print(overlapsContent);
    print(shrinkOffset);
    if (top == 0) {
      top = 0;
    } else if (top < 50) {
      top = 50 - top;
    } else {
      top = 50;
    }
    return Container(
      padding: EdgeInsets.only(top: top),
      height: double.infinity,
      color: Colors.grey,
      child: const Text('我是第二个'),
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    return ListTile(
      title: Text(index.toString()),
    );
  }
}
