import 'package:flutter/material.dart';

typedef SliverWidgetBuilder = Widget Function(
    BuildContext context, double shrinkOffset, bool overlapsContent);

class SliverWrapper extends StatelessWidget {
  final double minHeight;
  final double maxHeight;

  final bool floating;

  final bool pinned;

  final SliverWidgetBuilder builder;

  const SliverWrapper({
    super.key,
    this.minHeight = 52,
    this.maxHeight = 52,
    this.floating = false,
    this.pinned = false,
    required this.builder,
  });

  const SliverWrapper.height({
    super.key,
    double height = 52,
    this.floating = false,
    this.pinned = false,
    required this.builder,
  })  : minHeight = height,
        maxHeight = height;

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      delegate: _WrapperDelegate(
        maxExtent: maxHeight,
        minExtent: minHeight,
        builder: builder,
      ),
      floating: floating,
      pinned: pinned,
    );
  }
}

class _WrapperDelegate extends SliverPersistentHeaderDelegate {
  @override
  final double maxExtent;

  @override
  final double minExtent;

  final SliverWidgetBuilder builder;

  _WrapperDelegate({
    required this.maxExtent,
    required this.minExtent,
    required this.builder,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return builder(context, shrinkOffset, overlapsContent);
  }

  @override
  bool shouldRebuild(_WrapperDelegate old) {
    return old.builder != builder ||
        old.minExtent != maxExtent ||
        old.maxExtent != maxExtent;
  }
}
