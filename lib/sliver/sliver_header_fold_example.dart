import 'package:flutter/material.dart';

class SliverHeaderFoldExample extends StatefulWidget {
  const SliverHeaderFoldExample({super.key});

  @override
  State<SliverHeaderFoldExample> createState() =>
      _SliverHeaderFoldExampleState();
}

class _SliverHeaderFoldExampleState extends State<SliverHeaderFoldExample> {
  static const appBarHeight = 52.0;
  static const headerHeight = 152.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverHeaderFolder(
            builder: _buildHeader,
            min: appBarHeight,
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              _buildItem,
              childCount: 100,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    print('shrinkOffset = $shrinkOffset, overlapsContent = $overlapsContent');
    var currentPerfect = shrinkOffset / (headerHeight - appBarHeight);
    if (currentPerfect > 1) {
      currentPerfect = 1;
    } else if (currentPerfect < 0) {
      currentPerfect = 0;
    }
    // currentPerfect = 1 - currentPerfect;
    return Container(
      color: Colors.yellow,
      child: Column(
        children: <Widget>[
          Container(
            height: appBarHeight,
          ),
          const Flexible(
            child: ClipRect(
              child: Row(
                children: <Widget>[
                  Icon(Icons.person),
                  Icon(Icons.person),
                  Icon(Icons.person),
                  Icon(Icons.person),
                  Text("我是标题\n我是内容\n第三行")
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    return ListTile(
      title: Text(index.toString()),
    );
  }
}

typedef SliverFolderBuilder = Widget Function(
    BuildContext context, double shrinkOffset, bool overlapsContent);

class SliverHeaderFolder extends StatelessWidget {
  final double min;
  final double max;
  final SliverFolderBuilder builder;

  const SliverHeaderFolder({
    super.key,
    this.min = 52,
    this.max = 152,
    required this.builder,
  });

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      delegate: _FolderDelegate(
        minExtent: min,
        maxExtent: max,
        builder: builder,
      ),
      pinned: true,
    );
  }
}

class _FolderDelegate extends SliverPersistentHeaderDelegate {
  final SliverFolderBuilder builder;

  _FolderDelegate({
    required this.maxExtent,
    required this.minExtent,
    required this.builder,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return builder(context, shrinkOffset, overlapsContent);
  }

  @override
  final double maxExtent;

  @override
  final double minExtent;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
