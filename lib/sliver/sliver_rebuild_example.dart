import 'package:flutter/material.dart';
import 'package:flutter_caijinglong_examples/counter_example.dart';
import 'package:flutter_caijinglong_examples/sliver/sliver_header.dart';

class SliverRebuildExample extends StatefulWidget {
  const SliverRebuildExample({super.key});

  @override
  State<SliverRebuildExample> createState() => _SliverRebuildExampleState();
}

class _SliverRebuildExampleState extends State<SliverRebuildExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverWrapper(
            builder: (context, shrinkOffset, overlapsContent) {
              return PageView.builder(
                itemBuilder: (context, index) {
                  return const CounterPage();
                },
                itemCount: 5,
              );
            },
            minHeight: 150,
            maxHeight: 500,
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              _buildItem,
              childCount: 20,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    return ListTile(
      title: Text(index.toString()),
    );
  }
}
