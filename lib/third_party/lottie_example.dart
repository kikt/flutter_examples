import 'package:flutter/material.dart';
import 'package:flutter_caijinglong_examples/widget/my_app_bar.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter_caijinglong_examples/const/resource.dart';

class LottieExample extends StatefulWidget {
  const LottieExample({super.key});

  @override
  State<LottieExample> createState() => _LottieExampleState();
}

class _LottieExampleState extends State<LottieExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(child: widget),
      body: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemBuilder: (context, index) {
          return LottieBuilder.asset(R.ASSETS_LOTTIE_JSON);
        },
      ),
    );
  }
}
