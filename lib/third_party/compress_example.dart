import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_caijinglong_examples/const/resource.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_size_getter/image_size_getter.dart';
import 'package:mvimg/mvimg.dart';

class CompressExample extends StatefulWidget {
  const CompressExample({super.key});

  @override
  State<CompressExample> createState() => _CompressExampleState();
}

class _CompressExampleState extends State<CompressExample> {
  final StringBuffer _logBuffer = StringBuffer();

  String get _log => _logBuffer.toString();

  void writeLog(String log) {
    _logBuffer.writeln(log);
    setState(() {});
  }

  Future<void> _compressImage() async {
    final buffer = await rootBundle.load(R.ASSETS_2611736239025__PIC_JPG);
    final bytes = buffer.buffer.asUint8List();

    final srcSizeResult = ImageSizeGetter.getSizeResult(MemoryInput(bytes));
    final srcSize = srcSizeResult.size;
    final srcType = srcSizeResult.decoder.decoderName;
    writeLog('Source image size: ${srcSize.width}x${srcSize.height}');
    writeLog('Source image type: $srcType');

    final srcMvimg = Mvimg(BufferInput.memory(bytes));
    srcMvimg.decode();
    if (srcMvimg.isMvimg()) {
      writeLog('Source image is mvimg');
      final videoBytes = srcMvimg.getVideoBytes();
      writeLog('Video bytes length: ${videoBytes.length}');
    } else {
      writeLog('Source image is not mvimg');
    }

    final compressed = await FlutterImageCompress.compressWithList(bytes,
        format: CompressFormat.jpeg, quality: 80);
    writeLog('Compressed image size: ${compressed.length}');

    final sizeResult = ImageSizeGetter.getSizeResult(MemoryInput(compressed));
    final size = sizeResult.size;
    final type = sizeResult.decoder.decoderName;
    writeLog('Compressed image size: ${size.width}x${size.height}');
    writeLog('Compressed image type: $type');
  }

  void _clearLog() {
    _logBuffer.clear();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Compress Example'),
      ),
      body: Column(
        children: [
          Row(
            children: [
              ElevatedButton(
                onPressed: _compressImage,
                child: const Text('Compress Image'),
              ),
              ElevatedButton(
                onPressed: _clearLog,
                child: const Text('Clear Log'),
              ),
            ],
          ),
          Flexible(
            child: Text(_log),
          ),
        ],
      ),
    );
  }
}
