import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_caijinglong_examples/const/resource.dart';
import 'package:rive/rive.dart';

class RiveExample extends StatefulWidget {
  const RiveExample({super.key});

  @override
  State<RiveExample> createState() => _RiveExampleState();
}

class _RiveFakeAsset {
  final String key;
  final bool fake;
  final int startPosition;
  final int endPosition;

  const _RiveFakeAsset(
      this.key, this.fake, this.startPosition, this.endPosition);

  const _RiveFakeAsset.fromNormal(this.key)
      : fake = false,
        startPosition = 0,
        endPosition = 0;

  @override
  String toString() {
    return '_RiveFakeAsset{key: $key, fake: $fake, startPosition: $startPosition, endPosition: $endPosition}';
  }
}

class _RiveExampleState extends State<RiveExample> {
  static const assets = [
    _RiveFakeAsset.fromNormal(R.ASSETS_RIVE_NO_WATERFLOW_RIV),
    _RiveFakeAsset.fromNormal(R.ASSETS_RIVE_HAVE_WATERFLOW_RIV),
    _RiveFakeAsset(R.ASSETS_RIVE_HAVE_WATERFLOW_RIV, true, 48, 11537),
  ];

  _RiveFakeAsset? _asset;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Example for Rive"),
      ),
      body: Column(
        children: [
          for (final asset in assets)
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              width: double.infinity,
              height: 44,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    _asset = asset;
                  });
                },
                child: Text(
                  asset.toString(),
                  style: TextStyle(
                    color: _asset == asset ? Colors.yellowAccent : Colors.white,
                  ),
                ),
              ),
            ),
          if (_asset != null)
            Expanded(
              child: _RiveAsset(asset: _asset!),
            ),
        ],
      ),
    );
  }
}

class _RiveAsset extends StatelessWidget {
  const _RiveAsset({
    required this.asset,
  });

  final _RiveFakeAsset asset;

  @override
  Widget build(BuildContext context) {
    if (asset.fake) {
      return _RiveFaker(
        asset: asset.key,
        startPosition: asset.startPosition,
        endPosition: asset.endPosition,
      );
    } else {
      return RiveAnimation.asset(asset.key);
    }
  }
}

class _RiveFaker extends StatefulWidget {
  const _RiveFaker({
    required this.asset,
    required this.startPosition,
    required this.endPosition,
  });

  final String asset;
  final int startPosition;
  final int endPosition;

  @override
  State<_RiveFaker> createState() => _RiveFakerState();
}

class _RiveFakerState extends State<_RiveFaker> {
  RiveFile? riveFile;

  @override
  void initState() {
    super.initState();
    DefaultAssetBundle.of(context).load(widget.asset).then((value) {
      final list = value.buffer.asUint8List();
      // remove the between start and end position
      final firstList = list.sublist(0, widget.startPosition);
      final secondList = list.sublist(widget.endPosition);
      final newList = Uint8List.fromList(firstList + secondList);

      final buffer = ByteData.view(newList.buffer);

      riveFile = RiveFile.import(buffer);
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final file = riveFile;
    if (file == null) {
      return const SizedBox();
    }
    return RiveAnimation.direct(file);
  }
}
