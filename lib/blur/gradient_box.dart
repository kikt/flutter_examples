import 'package:flutter/material.dart';

class GradientBox extends StatelessWidget {
  const GradientBox({
    super.key,
    this.width = 200,
    this.height = 200,
  });

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Colors.blue,
            Colors.purple,
            Colors.red,
          ],
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      child: const Center(
        child: FlutterLogo(
          size: 100,
        ),
      ),
    );
  }
}
