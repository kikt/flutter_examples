import 'package:flutter/material.dart';
import 'dart:ui';
import 'gradient_box.dart';

class GaussianBlurExample extends StatefulWidget {
  const GaussianBlurExample({super.key});

  @override
  State<GaussianBlurExample> createState() => _GaussianBlurExampleState();
}

class _GaussianBlurExampleState extends State<GaussianBlurExample> {
  double _blurValue = 5.0;
  TileMode _tileMode = TileMode.clamp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('高斯模糊示例'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // TileMode selection
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Wrap(
                alignment: WrapAlignment.center,
                spacing: 8.0,
                runSpacing: 8.0,
                children: TileMode.values.map((mode) {
                  return Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Radio<TileMode>(
                        value: mode,
                        groupValue: _tileMode,
                        onChanged: (TileMode? value) {
                          if (value != null) {
                            setState(() {
                              _tileMode = value;
                            });
                          }
                        },
                      ),
                      Text(mode.name),
                    ],
                  );
                }).toList(),
              ),
            ),
            const SizedBox(height: 20),
            // 原始图像
            const GradientBox(),
            const SizedBox(height: 20),
            // 固定模糊效果的图像
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Stack(
                children: [
                  const GradientBox(),
                  Positioned.fill(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: 5.0,
                        sigmaY: 5.0,
                        tileMode: _tileMode,
                      ),
                      child: Container(
                        color: Colors.black.withOpacity(0),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            // 可调节的模糊效果
            Text('模糊程度: ${_blurValue.toStringAsFixed(1)}'),
            Slider(
              value: _blurValue,
              min: 0,
              max: 20,
              onChanged: (value) {
                setState(() {
                  _blurValue = value;
                });
              },
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Stack(
                children: [
                  SizedBox(
                    width: 200,
                    height: 200,
                    child: const GradientBox(),
                  ),
                  Positioned.fill(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: _blurValue,
                        sigmaY: _blurValue,
                        tileMode: _tileMode,
                      ),
                      child: Container(
                        color: Colors.black.withOpacity(0),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
