import 'dart:developer';

import 'package:flutter/material.dart';

class PageViewCannotScrollExample extends StatefulWidget {
  const PageViewCannotScrollExample({super.key});

  @override
  State<PageViewCannotScrollExample> createState() =>
      _PageViewCannotScrollExampleState();
}

class _PageViewCannotScrollExampleState
    extends State<PageViewCannotScrollExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: PageView.builder(
        itemBuilder: _buildItem,
        // physics: const NeverScrollableScrollPhysics(),
      ),
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    return MockVideoWidget(
      index: index,
    );
  }
}

class MockVideoWidget extends StatefulWidget {
  final int index;

  const MockVideoWidget({super.key, required this.index});

  @override
  MockVideoWidgetState createState() => MockVideoWidgetState();
}

class MockVideoWidgetState extends State<MockVideoWidget>
    with AutomaticKeepAliveClientMixin {
  bool isPlaying = false;

  @override
  void initState() {
    super.initState();
    log('${widget.index} initState');
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text('假装我是个播放器, 我的编号是${widget.index}'),
          IconButton(
            icon: isPlaying
                ? const Icon(Icons.stop)
                : const Icon(Icons.play_arrow),
            onPressed: () {
              setState(() {
                isPlaying = !isPlaying;
              });
            },
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    log('${widget.index} dispose');
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;
}
