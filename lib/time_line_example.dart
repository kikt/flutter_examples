import 'dart:math';

import 'package:flutter/material.dart';

class TimeLineExample extends StatefulWidget {
  const TimeLineExample({super.key});

  @override
  State<TimeLineExample> createState() => _TimeLineExampleState();
}

class _TimeLineExampleState extends State<TimeLineExample> {
  final baseDt = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.runtimeType.toString()),
      ),
      body: ListView.builder(
        itemBuilder: _buildItem,
      ),
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    final dt = baseDt.add(Duration(days: index));
    final title =
        "${dt.year.toString().padLeft(4, '0')}-${dt.month.toString().padLeft(2, '0')}-${dt.day.toString().padLeft(2, '0')}";

    final text = "我是字" * Random().nextInt(30);

    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey, width: 0.5),
      ),
      padding: const EdgeInsets.all(10),
      child: Row(
        children: <Widget>[
          Text(title),
          Container(
            width: 10,
          ),
          Expanded(child: Text(text)),
        ],
      ),
    );
  }
}
