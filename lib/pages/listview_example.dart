import 'package:flutter/material.dart';

class ListViewExample extends StatefulWidget {
  const ListViewExample({super.key});

  @override
  State<ListViewExample> createState() => _ListViewExampleState();
}

class _ListViewExampleState extends State<ListViewExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(itemBuilder: _buildItem),
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    return SizedBox(
      height: 30,
      child: Text('$index'),
    );
  }
}
