import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TimerNotifier extends ChangeNotifier {
  late final Timer _timer;

  TimerNotifier() {
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      notifyListeners();
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}

class ItemEntity {
  final DateTime dt;

  ItemEntity(this.dt);
}

class CountDownPage extends StatefulWidget {
  const CountDownPage({super.key});

  @override
  State<CountDownPage> createState() => _CountDownPageState();
}

class _CountDownPageState extends State<CountDownPage> {
  final items = <ItemEntity>[];

  @override
  void initState() {
    super.initState();
    for (var i = 0; i < 50; i++) {
      items.add(
          ItemEntity(DateTime.now().add(Duration(seconds: 30 * i + 36000))));
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: TimerNotifier(),
      builder: (context, child) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('count down'),
          ),
          body: ListView.builder(
            itemBuilder: _buildItem,
            itemCount: items.length,
          ),
        );
      },
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    final item = items[index];
    return _Item(
      item: item,
    );
  }
}

String _formatText(Duration duration) {
  final h = duration.inHours.toString().padLeft(2, '0');
  final m = (duration.inMinutes % 60).toString().padLeft(2, '0');
  final s = (duration.inSeconds % 60).toString().padLeft(2, '0');
  return '$h:$m:$s';
}

class _Item extends StatelessWidget {
  final ItemEntity item;

  const _Item({
    required this.item,
  });

  @override
  Widget build(BuildContext context) {
    return Consumer<TimerNotifier>(
      builder: (context, value, child) {
        final now = DateTime.now();
        final duration = item.dt.difference(now);
        final newText = _formatText(duration);
        return ListTile(
          title: const Text('剩余时间'),
          subtitle: Text(newText),
        );
      },
    );
  }
}

// class _Item extends StatefulWidget {
//   final ItemEntity item;

//   const _Item({
//     Key? key,
//     required this.item,
//   }) : super(key: key);
//   @override
//   __ItemState createState() => __ItemState();
// }

// class __ItemState extends State<_Item> with SingleTickerProviderStateMixin {
//   final ValueNotifier<String> textValueNotifier = ValueNotifier('');

//   Ticker? ticker;
//   Timer? timer;
//   @override
//   void initState() {
//     super.initState();

//     // ticker = createTicker((elapsed) {
//     //   onTrip();
//     // });
//     // ticker?.start();

//     // timer = Timer.periodic(Duration(milliseconds: 250), (timer) {
//     //   onTrip();
//     // });
//   }

//   @override
//   void dispose() {
//     // ticker?.dispose();
//     timer?.cancel();
//     super.dispose();
//   }

//   void onTrip() {
//     final now = DateTime.now();
//     final duration = widget.item.dt.difference(now);

//     final newText = _formatText(duration);

//     if (newText != textValueNotifier.value) {
//       textValueNotifier.value = newText;
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return ListTile(
//       title: Text('剩余时间'),
//       subtitle: _buildTime(),
//     );
//   }
// }
