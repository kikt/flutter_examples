import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class InputTagExample extends StatefulWidget {
  const InputTagExample({super.key});

  @override
  State<InputTagExample> createState() => _InputTagExampleState();
}

class _InputTagExampleState extends State<InputTagExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('InputTag'),
      ),
      body: ListView(
        children: [
          CustomInputTags(),
        ],
      ),
    );
  }
}

class CustomInputTags extends StatefulWidget {
  const CustomInputTags({super.key});

  @override
  State<CustomInputTags> createState() => CustomInputTagsState();
}

class CustomInputTagsState extends State<CustomInputTags> {
  final List<String> _tags = <String>[];

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        for (final tag in _tags) _buildTag(tag),
        _buildInput(),
      ],
    );
  }

  Widget _buildTag(String tag) {
    return InputChip(
      label: Text(tag),
      onDeleted: () {
        setState(() {
          _tags.remove(tag);
        });
      },
    );
  }

  TextEditingController controller = TextEditingController();

  Widget _buildInput() {
    // return TextField(
    //   onSubmitted: (value) {
    //     setState(() {
    //       _tags.add(value);
    //     });
    //   },
    // );
    return RawKeyboardListener(
      focusNode: FocusNode(),
      // onKeyEvent: (value) {
      //   print(value);
      // },
      onKey: (value){
        print(value);
      },
      child: EditableText(
        controller: controller,
        focusNode: FocusNode(),
        style: const TextStyle(color: Colors.black, fontSize: 12),
        cursorColor: Colors.blue,
        backgroundCursorColor: Colors.blue,
        onChanged: (value) {
          print(value);
        },
      ),
    );
  }
}
