/// Generate by [asset_generator](https://github.com/fluttercandies/flutter_asset_generator) library.
/// PLEASE DO NOT EDIT MANUALLY.
// ignore_for_file: constant_identifier_names
class R {
  const R._();

  /// ![preview](file:///Users/cai/code/flutter/examples/flutter_examples/assets/2611736239025_.pic.jpg)
  static const String ASSETS_2611736239025__PIC_JPG =
      'assets/2611736239025_.pic.jpg';

  /// ![preview](file:///Users/cai/code/flutter/examples/flutter_examples/assets/lottie.json)
  static const String ASSETS_LOTTIE_JSON = 'assets/lottie.json';

  /// ![preview](file:///Users/cai/code/flutter/examples/flutter_examples/assets/lottie/glass.json)
  static const String ASSETS_LOTTIE_GLASS_JSON = 'assets/lottie/glass.json';

  /// ![preview](file:///Users/cai/code/flutter/examples/flutter_examples/assets/lottie/images/img_0.png)
  static const String ASSETS_LOTTIE_IMAGES_IMG_0_PNG =
      'assets/lottie/images/img_0.png';

  /// ![preview](file:///Users/cai/code/flutter/examples/flutter_examples/assets/rive/have.log.txt)
  static const String ASSETS_RIVE_HAVE_LOG_TXT = 'assets/rive/have.log.txt';

  /// ![preview](file:///Users/cai/code/flutter/examples/flutter_examples/assets/rive/have_waterflow.riv)
  static const String ASSETS_RIVE_HAVE_WATERFLOW_RIV =
      'assets/rive/have_waterflow.riv';

  /// ![preview](file:///Users/cai/code/flutter/examples/flutter_examples/assets/rive/have_waterflow_fake.riv)
  static const String ASSETS_RIVE_HAVE_WATERFLOW_FAKE_RIV =
      'assets/rive/have_waterflow_fake.riv';

  /// ![preview](file:///Users/cai/code/flutter/examples/flutter_examples/assets/rive/no.log.txt)
  static const String ASSETS_RIVE_NO_LOG_TXT = 'assets/rive/no.log.txt';

  /// ![preview](file:///Users/cai/code/flutter/examples/flutter_examples/assets/rive/no_waterflow.riv)
  static const String ASSETS_RIVE_NO_WATERFLOW_RIV =
      'assets/rive/no_waterflow.riv';
}
