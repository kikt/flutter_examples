import 'package:flutter/material.dart';
import 'package:flutter_caijinglong_examples/ui_helper.dart';

class CalcWidgetSizeExample extends StatefulWidget {
  const CalcWidgetSizeExample({super.key});

  @override
  State<CalcWidgetSizeExample> createState() => _CalcWidgetSizeExampleState();
}

class _CalcWidgetSizeExampleState extends State<CalcWidgetSizeExample> {
  List<Widget> get widgets => [
        const Text('dsljfl;kdsjf;ldsjklfj'),
      ];

  BoxConstraints get boxConstraints => const BoxConstraints(
        maxWidth: 200,
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("calc widget size example"),
      ),
      body: Column(
        children: widgets.map((e) => _buildButton(e)).toList(),
      ),
    );
  }

  Widget _buildButton(Widget needMeatureWidget) {
    return ElevatedButton(
      onPressed: () async {
        final rect = await UIHelper.measureWidgetRect(
          context: context,
          widget: needMeatureWidget,
          boxConstraints: boxConstraints,
        );

        print('size: ${rect.size}');
      },
      child: Text(needMeatureWidget.toStringShort()),
    );
  }
}
