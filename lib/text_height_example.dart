import 'package:flutter/material.dart';

/// 顶部对齐的example
class TextAndImageAlignTopExample extends StatefulWidget {
  const TextAndImageAlignTopExample({super.key});

  @override
  State<TextAndImageAlignTopExample> createState() =>
      _TextAndImageAlignTopExampleState();
}

class _TextAndImageAlignTopExampleState
    extends State<TextAndImageAlignTopExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.runtimeType.toString()),
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Flexible(
                  child: Text(
                    '你好\n你好',
                    style: TextStyle(
                      textBaseline: TextBaseline.alphabetic,
                      height: 1.1,
                      fontSize: 100,
                    ),
                    // style: TextStyle(textBaseline: TextBaseline.ideographic),
                  ),
                ),
                Image.network(
                  'https://cdn.jsdelivr.net/gh/kikt-blog/test_images@master/test-header.jpg',
                  width: 200,
                  height: 200,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
