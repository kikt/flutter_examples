import 'package:flutter/material.dart';

class InputTextWrapper extends StatefulWidget {
  final String text;
  final ValueChanged<String> onChanged;

  const InputTextWrapper({
    super.key,
    required this.text,
    required this.onChanged,
  });

  @override
  State<InputTextWrapper> createState() => _InputTextWrapperState();
}

class _InputTextWrapperState extends State<InputTextWrapper> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: const Text('Name'),
      subtitle: Text(widget.text),
      trailing: const Icon(
        Icons.arrow_forward_ios,
        size: 14,
      ),
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => InputTextPage(
              text: widget.text,
              onChanged: (value) {
                if (value != null) {
                  widget.onChanged(value);
                }
              },
            ),
          ),
        );
      },
    );
  }
}

class InputTextPage extends StatefulWidget {
  const InputTextPage({
    super.key,
    required this.text,
    required this.onChanged,
  });

  final String text;
  final ValueChanged<String?> onChanged;

  @override
  State<InputTextPage> createState() => _InputTextPageState();
}

class _InputTextPageState extends State<InputTextPage> {
  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController(text: widget.text);
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextField(
              controller: controller,
            ),
            ElevatedButton(
              onPressed: () {
                widget.onChanged(controller.text);
                Navigator.of(context).pop();
              },
              child: const Text('Submit'),
            ),
          ],
        ),
      ),
    );
  }
}
