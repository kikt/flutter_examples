import 'package:flutter/material.dart';
import 'package:flutter_caijinglong_examples/blur/gaussian_blur_example.dart';
import 'package:flutter_caijinglong_examples/calc_size/calc_size_page.dart';
import 'package:flutter_caijinglong_examples/canvas/export_canvas.dart';
import 'package:flutter_caijinglong_examples/canvas/text_canvas_calc.dart';
import 'package:flutter_caijinglong_examples/edit_image/edit_widget_example.dart';
import 'package:flutter_caijinglong_examples/edit_image/edit_example.dart';
import 'package:flutter_caijinglong_examples/form/form_submit_example.dart';
import 'package:flutter_caijinglong_examples/page_view_cannot_scroll_example.dart';
import 'package:flutter_caijinglong_examples/pages/count_down_page.dart';
import 'package:flutter_caijinglong_examples/project/count_down.dart';
import 'package:flutter_caijinglong_examples/rive/rive_example.dart';
import 'package:flutter_caijinglong_examples/sliver/sliver_category_example.dart';
import 'package:flutter_caijinglong_examples/sliver/sliver_header_fold_example.dart';
import 'package:flutter_caijinglong_examples/sliver/sliver_rebuild_example.dart';
import 'package:flutter_caijinglong_examples/text_height_example.dart';
import 'package:flutter_caijinglong_examples/third_party/lottie_example.dart';
import 'package:flutter_caijinglong_examples/widget/example_button.dart';

import 'gesture/expand_click_area.dart';
import 'gridview_by_listview.dart';
import 'input/input_tag.dart';
import 'list_wheel_example.dart';
import 'multi_textfield_example.dart';
import 'nav2/navigator_example.dart';
import 'pages/listview_example.dart';
import 'soft_input_bottom/soft_input_widget.dart';
import 'third_party/compress_example.dart';
import 'time_line_example.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const IndexPage(),
    );
  }
}

class IndexPage extends StatefulWidget {
  const IndexPage({super.key});

  @override
  State<IndexPage> createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  List<Widget> providerExamples() {
    return const [
      GaussianBlurExample(),
      InputTagExample(),
      RiveExample(),
      ListWheelExample(),
      TimeLineExample(),
      GridViewByListViewExample(),
      MultiTextFieldExample(),
      SliverHeaderFoldExample(),
      TextAndImageAlignTopExample(),
      SliverCategoryExample(),
      SliverRebuildExample(),
      PageViewCannotScrollExample(),
      ExpandClickArea(),
      ExportCanvas(),
      LottieExample(),
      // SliverListExample(),
      NavigatorExample(),
      CountDownPage(),
      ListViewExample(),
      FormSubmitExample(),
      EditExample(),
      EditWidgetExample(),
      TextCanvasCalcDart(),
      CalcWidgetSizeExample(),
      SoftInputPage(),
      CountdownPageExample(),
      CompressExample(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final examples = providerExamples();
    return Scaffold(
      appBar: AppBar(
        title: const Text("home page"),
      ),
      backgroundColor: Colors.grey[200],
      body: ListView.builder(
        padding: const EdgeInsets.only(left: 12, right: 12, bottom: 20),
        itemCount: examples.length,
        itemBuilder: (context, index) {
          final example = examples[index];
          final title = example.runtimeType.toString();
          return ExampleButton(
            title: title,
            child: example,
          );
        },
      ),
    );
  }
}
