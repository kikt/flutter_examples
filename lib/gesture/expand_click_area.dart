import 'package:flutter/material.dart';
import 'package:flutter_caijinglong_examples/widget/my_app_bar.dart';

class ExpandClickArea extends StatefulWidget {
  const ExpandClickArea({super.key});

  @override
  State<ExpandClickArea> createState() => _ExpandClickAreaState();
}

class _ExpandClickAreaState extends State<ExpandClickArea> {
  final imageUrl =
      'https://cdn.jsdelivr.net/gh/kikt-blog/test_images/test-header.jpg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(child: widget),
      body: Column(
        children: <Widget>[
          const Text('No finish'),
          _buildGoodItem(),
        ],
      ),
    );
  }

  Widget _buildGoodItem() {
    return SizedBox(
      height: 80,
      child: Row(
        children: <Widget>[
          Container(
            width: 20,
          ),
          Image.network(
            imageUrl,
            width: 40,
            height: 40,
          ),
          Expanded(
            child: Container(),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                height: 10,
              ),
              Text(
                '注册时间: ${DateTime.now().toLocal().toString()}',
              ),
              Container(
                height: 10,
              ),
              TapWrapper(
                onTap: () {
                  print('click heart');
                },
                child: const Icon(
                  Icons.favorite,
                ),
              ),
            ],
          ),
          Container(
            width: 20,
          ),
        ],
      ),
    );
  }
}

class TapWrapper extends StatelessWidget {
  final Function onTap;
  final Widget child;
  final Size size;

  const TapWrapper({
    super.key,
    required this.onTap,
    required this.child,
    this.size = const Size(40, 40),
  });
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        child,
      ],
    );
  }
}
