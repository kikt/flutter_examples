import 'package:flutter/material.dart';

class ExampleButton extends StatelessWidget {
  const ExampleButton({
    super.key,
    required this.child,
    required this.title,
  });

  final Widget child;
  final String title;

  @override
  Widget build(BuildContext context) {
    const double padding = 8;
    const double radius = 8;
    const double height = 32;
    const double fontSize = 14;
    return Padding(
      padding: const EdgeInsets.only(
        left: padding,
        right: padding,
        top: padding,
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(radius),
        child: Material(
          color: Theme.of(context).primaryColor,
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => child,
                ),
              );
            },
            child: SizedBox(
              height: height,
              child: Center(
                child: Text(
                  title,
                  style: const TextStyle(
                    fontSize: fontSize,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
