import 'package:flutter/material.dart';

class MyAppBar extends PreferredSize {
  const MyAppBar({
    super.key,
    required super.child,
  }) : super(
          preferredSize: const Size.fromHeight(kToolbarHeight),
        );

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(child.runtimeType.toString()),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
