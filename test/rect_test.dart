import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Rect contains', () {
    // final rect = Rect.fromLTWH(100, 100, 100, 100);
    const rect = Rect.fromLTRB(200, 100, 100, 100);
    print(rect);

    expect(rect.contains(const Offset(99, 99)), false);
    expect(rect.contains(const Offset(100, 100)), true);
    expect(rect.contains(const Offset(199, 199)), true);
  });
}
