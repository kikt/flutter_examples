enum CoreFieldType {stringType, uintType, doubleType, boolType, colorType, bytesType, nullType}

/// convert field type to value type
CoreFieldType convertFieldTypeToEnum(int value) {
  switch (value) {
    case 4: // ComponentBase.namePropertyKey
      return CoreFieldType.stringType;
    case 55: // AnimationBase.namePropertyKey
      return CoreFieldType.stringType;
    case 138: // StateMachineComponentBase.namePropertyKey
      return CoreFieldType.stringType;
    case 280: // KeyFrameStringBase.valuePropertyKey
      return CoreFieldType.stringType;
    case 248: // OpenUrlEventBase.urlPropertyKey
      return CoreFieldType.stringType;
    case 268: // TextValueRunBase.textPropertyKey
      return CoreFieldType.stringType;
    case 246: // CustomPropertyStringBase.propertyValuePropertyKey
      return CoreFieldType.stringType;
    case 203: // AssetBase.namePropertyKey
      return CoreFieldType.stringType;
    case 362: // FileAssetBase.cdnBaseUrlPropertyKey
      return CoreFieldType.stringType;
    case 5: // ComponentBase.parentIdPropertyKey
      return CoreFieldType.uintType;
    case 119: // DrawTargetBase.drawableIdPropertyKey
      return CoreFieldType.uintType;
    case 120: // DrawTargetBase.placementValuePropertyKey
      return CoreFieldType.uintType;
    case 173: // TargetedConstraintBase.targetIdPropertyKey
      return CoreFieldType.uintType;
    case 178: // DistanceConstraintBase.modeValuePropertyKey
      return CoreFieldType.uintType;
    case 179: // TransformSpaceConstraintBase.sourceSpaceValuePropertyKey
      return CoreFieldType.uintType;
    case 180: // TransformSpaceConstraintBase.destSpaceValuePropertyKey
      return CoreFieldType.uintType;
    case 195: // TransformComponentConstraintBase.minMaxSpaceValuePropertyKey
      return CoreFieldType.uintType;
    case 175: // IKConstraintBase.parentBoneCountPropertyKey
      return CoreFieldType.uintType;
    case 23: // DrawableBase.blendModeValuePropertyKey
      return CoreFieldType.uintType;
    case 129: // DrawableBase.drawableFlagsPropertyKey
      return CoreFieldType.uintType;
    case 197: // NestedArtboardBase.artboardIdPropertyKey
      return CoreFieldType.uintType;
    case 198: // NestedAnimationBase.animationIdPropertyKey
      return CoreFieldType.uintType;
    case 296: // SoloBase.activeComponentIdPropertyKey
      return CoreFieldType.uintType;
    case 389: // ListenerFireEventBase.eventIdPropertyKey
      return CoreFieldType.uintType;
    case 56: // LinearAnimationBase.fpsPropertyKey
      return CoreFieldType.uintType;
    case 57: // LinearAnimationBase.durationPropertyKey
      return CoreFieldType.uintType;
    case 59: // LinearAnimationBase.loopValuePropertyKey
      return CoreFieldType.uintType;
    case 60: // LinearAnimationBase.workStartPropertyKey
      return CoreFieldType.uintType;
    case 61: // LinearAnimationBase.workEndPropertyKey
      return CoreFieldType.uintType;
    case 227: // ListenerInputChangeBase.inputIdPropertyKey
      return CoreFieldType.uintType;
    case 149: // AnimationStateBase.animationIdPropertyKey
      return CoreFieldType.uintType;
    case 237: // NestedInputBase.inputIdPropertyKey
      return CoreFieldType.uintType;
    case 51: // KeyedObjectBase.objectIdPropertyKey
      return CoreFieldType.uintType;
    case 165: // BlendAnimationBase.animationIdPropertyKey
      return CoreFieldType.uintType;
    case 168: // BlendAnimationDirectBase.inputIdPropertyKey
      return CoreFieldType.uintType;
    case 298: // BlendAnimationDirectBase.blendSourcePropertyKey
      return CoreFieldType.uintType;
    case 155: // TransitionConditionBase.inputIdPropertyKey
      return CoreFieldType.uintType;
    case 53: // KeyedPropertyBase.propertyKeyPropertyKey
      return CoreFieldType.uintType;
    case 224: // StateMachineListenerBase.targetIdPropertyKey
      return CoreFieldType.uintType;
    case 225: // StateMachineListenerBase.listenerTypeValuePropertyKey
      return CoreFieldType.uintType;
    case 67: // KeyFrameBase.framePropertyKey
      return CoreFieldType.uintType;
    case 68: // KeyFrameBase.interpolationTypePropertyKey
      return CoreFieldType.uintType;
    case 69: // KeyFrameBase.interpolatorIdPropertyKey
      return CoreFieldType.uintType;
    case 122: // KeyFrameIdBase.valuePropertyKey
      return CoreFieldType.uintType;
    case 228: // ListenerBoolChangeBase.valuePropertyKey
      return CoreFieldType.uintType;
    case 240: // ListenerAlignTargetBase.targetIdPropertyKey
      return CoreFieldType.uintType;
    case 156: // TransitionValueConditionBase.opValuePropertyKey
      return CoreFieldType.uintType;
    case 151: // StateTransitionBase.stateToIdPropertyKey
      return CoreFieldType.uintType;
    case 152: // StateTransitionBase.flagsPropertyKey
      return CoreFieldType.uintType;
    case 158: // StateTransitionBase.durationPropertyKey
      return CoreFieldType.uintType;
    case 160: // StateTransitionBase.exitTimePropertyKey
      return CoreFieldType.uintType;
    case 349: // StateTransitionBase.interpolationTypePropertyKey
      return CoreFieldType.uintType;
    case 350: // StateTransitionBase.interpolatorIdPropertyKey
      return CoreFieldType.uintType;
    case 392: // StateMachineFireEventBase.eventIdPropertyKey
      return CoreFieldType.uintType;
    case 393: // StateMachineFireEventBase.occursValuePropertyKey
      return CoreFieldType.uintType;
    case 167: // BlendState1DBase.inputIdPropertyKey
      return CoreFieldType.uintType;
    case 171: // BlendStateTransitionBase.exitBlendAnimationIdPropertyKey
      return CoreFieldType.uintType;
    case 48: // StrokeBase.capPropertyKey
      return CoreFieldType.uintType;
    case 49: // StrokeBase.joinPropertyKey
      return CoreFieldType.uintType;
    case 117: // TrimPathBase.modeValuePropertyKey
      return CoreFieldType.uintType;
    case 40: // FillBase.fillRulePropertyKey
      return CoreFieldType.uintType;
    case 128: // PathBase.pathFlagsPropertyKey
      return CoreFieldType.uintType;
    case 102: // WeightBase.valuesPropertyKey
      return CoreFieldType.uintType;
    case 103: // WeightBase.indicesPropertyKey
      return CoreFieldType.uintType;
    case 110: // CubicWeightBase.inValuesPropertyKey
      return CoreFieldType.uintType;
    case 111: // CubicWeightBase.inIndicesPropertyKey
      return CoreFieldType.uintType;
    case 112: // CubicWeightBase.outValuesPropertyKey
      return CoreFieldType.uintType;
    case 113: // CubicWeightBase.outIndicesPropertyKey
      return CoreFieldType.uintType;
    case 92: // ClippingShapeBase.sourceIdPropertyKey
      return CoreFieldType.uintType;
    case 93: // ClippingShapeBase.fillRulePropertyKey
      return CoreFieldType.uintType;
    case 125: // PolygonBase.pointsPropertyKey
      return CoreFieldType.uintType;
    case 206: // ImageBase.assetIdPropertyKey
      return CoreFieldType.uintType;
    case 121: // DrawRulesBase.drawTargetIdPropertyKey
      return CoreFieldType.uintType;
    case 236: // ArtboardBase.defaultStateMachineIdPropertyKey
      return CoreFieldType.uintType;
    case 301: // JoystickBase.xIdPropertyKey
      return CoreFieldType.uintType;
    case 302: // JoystickBase.yIdPropertyKey
      return CoreFieldType.uintType;
    case 312: // JoystickBase.joystickFlagsPropertyKey
      return CoreFieldType.uintType;
    case 313: // JoystickBase.handleSourceIdPropertyKey
      return CoreFieldType.uintType;
    case 249: // OpenUrlEventBase.targetValuePropertyKey
      return CoreFieldType.uintType;
    case 95: // TendonBase.boneIdPropertyKey
      return CoreFieldType.uintType;
    case 316: // TextModifierRangeBase.unitsValuePropertyKey
      return CoreFieldType.uintType;
    case 325: // TextModifierRangeBase.typeValuePropertyKey
      return CoreFieldType.uintType;
    case 326: // TextModifierRangeBase.modeValuePropertyKey
      return CoreFieldType.uintType;
    case 378: // TextModifierRangeBase.runIdPropertyKey
      return CoreFieldType.uintType;
    case 356: // TextStyleFeatureBase.tagPropertyKey
      return CoreFieldType.uintType;
    case 357: // TextStyleFeatureBase.featureValuePropertyKey
      return CoreFieldType.uintType;
    case 320: // TextVariationModifierBase.axisTagPropertyKey
      return CoreFieldType.uintType;
    case 335: // TextModifierGroupBase.modifierFlagsPropertyKey
      return CoreFieldType.uintType;
    case 279: // TextStyleBase.fontAssetIdPropertyKey
      return CoreFieldType.uintType;
    case 289: // TextStyleAxisBase.tagPropertyKey
      return CoreFieldType.uintType;
    case 281: // TextBase.alignValuePropertyKey
      return CoreFieldType.uintType;
    case 284: // TextBase.sizingValuePropertyKey
      return CoreFieldType.uintType;
    case 287: // TextBase.overflowValuePropertyKey
      return CoreFieldType.uintType;
    case 377: // TextBase.originValuePropertyKey
      return CoreFieldType.uintType;
    case 272: // TextValueRunBase.styleIdPropertyKey
      return CoreFieldType.uintType;
    case 204: // FileAssetBase.assetIdPropertyKey
      return CoreFieldType.uintType;
    case 243: // CustomPropertyNumberBase.propertyValuePropertyKey
      return CoreFieldType.doubleType;
    case 172: // ConstraintBase.strengthPropertyKey
      return CoreFieldType.doubleType;
    case 177: // DistanceConstraintBase.distancePropertyKey
      return CoreFieldType.doubleType;
    case 182: // TransformComponentConstraintBase.copyFactorPropertyKey
      return CoreFieldType.doubleType;
    case 183: // TransformComponentConstraintBase.minValuePropertyKey
      return CoreFieldType.doubleType;
    case 184: // TransformComponentConstraintBase.maxValuePropertyKey
      return CoreFieldType.doubleType;
    case 185: // TransformComponentConstraintYBase.copyFactorYPropertyKey
      return CoreFieldType.doubleType;
    case 186: // TransformComponentConstraintYBase.minValueYPropertyKey
      return CoreFieldType.doubleType;
    case 187: // TransformComponentConstraintYBase.maxValueYPropertyKey
      return CoreFieldType.doubleType;
    case 363: // FollowPathConstraintBase.distancePropertyKey
      return CoreFieldType.doubleType;
    case 372: // TransformConstraintBase.originXPropertyKey
      return CoreFieldType.doubleType;
    case 373: // TransformConstraintBase.originYPropertyKey
      return CoreFieldType.doubleType;
    case 18: // WorldTransformComponentBase.opacityPropertyKey
      return CoreFieldType.doubleType;
    case 15: // TransformComponentBase.rotationPropertyKey
      return CoreFieldType.doubleType;
    case 16: // TransformComponentBase.scaleXPropertyKey
      return CoreFieldType.doubleType;
    case 17: // TransformComponentBase.scaleYPropertyKey
      return CoreFieldType.doubleType;
    case 13: // NodeBase.xPropertyKey
      return CoreFieldType.doubleType;
    case 14: // NodeBase.yPropertyKey
      return CoreFieldType.doubleType;
    case 58: // LinearAnimationBase.speedPropertyKey
      return CoreFieldType.doubleType;
    case 200: // NestedLinearAnimationBase.mixPropertyKey
      return CoreFieldType.doubleType;
    case 199: // NestedSimpleAnimationBase.speedPropertyKey
      return CoreFieldType.doubleType;
    case 292: // AdvanceableStateBase.speedPropertyKey
      return CoreFieldType.doubleType;
    case 297: // BlendAnimationDirectBase.mixValuePropertyKey
      return CoreFieldType.doubleType;
    case 140: // StateMachineNumberBase.valuePropertyKey
      return CoreFieldType.doubleType;
    case 63: // CubicInterpolatorBase.x1PropertyKey
      return CoreFieldType.doubleType;
    case 64: // CubicInterpolatorBase.y1PropertyKey
      return CoreFieldType.doubleType;
    case 65: // CubicInterpolatorBase.x2PropertyKey
      return CoreFieldType.doubleType;
    case 66: // CubicInterpolatorBase.y2PropertyKey
      return CoreFieldType.doubleType;
    case 157: // TransitionNumberConditionBase.valuePropertyKey
      return CoreFieldType.doubleType;
    case 337: // CubicInterpolatorComponentBase.x1PropertyKey
      return CoreFieldType.doubleType;
    case 338: // CubicInterpolatorComponentBase.y1PropertyKey
      return CoreFieldType.doubleType;
    case 339: // CubicInterpolatorComponentBase.x2PropertyKey
      return CoreFieldType.doubleType;
    case 340: // CubicInterpolatorComponentBase.y2PropertyKey
      return CoreFieldType.doubleType;
    case 229: // ListenerNumberChangeBase.valuePropertyKey
      return CoreFieldType.doubleType;
    case 70: // KeyFrameDoubleBase.valuePropertyKey
      return CoreFieldType.doubleType;
    case 239: // NestedNumberBase.nestedValuePropertyKey
      return CoreFieldType.doubleType;
    case 166: // BlendAnimation1DBase.valuePropertyKey
      return CoreFieldType.doubleType;
    case 202: // NestedRemapAnimationBase.timePropertyKey
      return CoreFieldType.doubleType;
    case 42: // LinearGradientBase.startXPropertyKey
      return CoreFieldType.doubleType;
    case 33: // LinearGradientBase.startYPropertyKey
      return CoreFieldType.doubleType;
    case 34: // LinearGradientBase.endXPropertyKey
      return CoreFieldType.doubleType;
    case 35: // LinearGradientBase.endYPropertyKey
      return CoreFieldType.doubleType;
    case 46: // LinearGradientBase.opacityPropertyKey
      return CoreFieldType.doubleType;
    case 47: // StrokeBase.thicknessPropertyKey
      return CoreFieldType.doubleType;
    case 39: // GradientStopBase.positionPropertyKey
      return CoreFieldType.doubleType;
    case 114: // TrimPathBase.startPropertyKey
      return CoreFieldType.doubleType;
    case 115: // TrimPathBase.endPropertyKey
      return CoreFieldType.doubleType;
    case 116: // TrimPathBase.offsetPropertyKey
      return CoreFieldType.doubleType;
    case 24: // VertexBase.xPropertyKey
      return CoreFieldType.doubleType;
    case 25: // VertexBase.yPropertyKey
      return CoreFieldType.doubleType;
    case 215: // MeshVertexBase.uPropertyKey
      return CoreFieldType.doubleType;
    case 216: // MeshVertexBase.vPropertyKey
      return CoreFieldType.doubleType;
    case 26: // StraightVertexBase.radiusPropertyKey
      return CoreFieldType.doubleType;
    case 79: // CubicAsymmetricVertexBase.rotationPropertyKey
      return CoreFieldType.doubleType;
    case 80: // CubicAsymmetricVertexBase.inDistancePropertyKey
      return CoreFieldType.doubleType;
    case 81: // CubicAsymmetricVertexBase.outDistancePropertyKey
      return CoreFieldType.doubleType;
    case 20: // ParametricPathBase.widthPropertyKey
      return CoreFieldType.doubleType;
    case 21: // ParametricPathBase.heightPropertyKey
      return CoreFieldType.doubleType;
    case 123: // ParametricPathBase.originXPropertyKey
      return CoreFieldType.doubleType;
    case 124: // ParametricPathBase.originYPropertyKey
      return CoreFieldType.doubleType;
    case 31: // RectangleBase.cornerRadiusTLPropertyKey
      return CoreFieldType.doubleType;
    case 161: // RectangleBase.cornerRadiusTRPropertyKey
      return CoreFieldType.doubleType;
    case 162: // RectangleBase.cornerRadiusBLPropertyKey
      return CoreFieldType.doubleType;
    case 163: // RectangleBase.cornerRadiusBRPropertyKey
      return CoreFieldType.doubleType;
    case 82: // CubicMirroredVertexBase.rotationPropertyKey
      return CoreFieldType.doubleType;
    case 83: // CubicMirroredVertexBase.distancePropertyKey
      return CoreFieldType.doubleType;
    case 126: // PolygonBase.cornerRadiusPropertyKey
      return CoreFieldType.doubleType;
    case 127: // StarBase.innerRadiusPropertyKey
      return CoreFieldType.doubleType;
    case 380: // ImageBase.originXPropertyKey
      return CoreFieldType.doubleType;
    case 381: // ImageBase.originYPropertyKey
      return CoreFieldType.doubleType;
    case 84: // CubicDetachedVertexBase.inRotationPropertyKey
      return CoreFieldType.doubleType;
    case 85: // CubicDetachedVertexBase.inDistancePropertyKey
      return CoreFieldType.doubleType;
    case 86: // CubicDetachedVertexBase.outRotationPropertyKey
      return CoreFieldType.doubleType;
    case 87: // CubicDetachedVertexBase.outDistancePropertyKey
      return CoreFieldType.doubleType;
    case 7: // ArtboardBase.widthPropertyKey
      return CoreFieldType.doubleType;
    case 8: // ArtboardBase.heightPropertyKey
      return CoreFieldType.doubleType;
    case 9: // ArtboardBase.xPropertyKey
      return CoreFieldType.doubleType;
    case 10: // ArtboardBase.yPropertyKey
      return CoreFieldType.doubleType;
    case 11: // ArtboardBase.originXPropertyKey
      return CoreFieldType.doubleType;
    case 12: // ArtboardBase.originYPropertyKey
      return CoreFieldType.doubleType;
    case 299: // JoystickBase.xPropertyKey
      return CoreFieldType.doubleType;
    case 300: // JoystickBase.yPropertyKey
      return CoreFieldType.doubleType;
    case 303: // JoystickBase.posXPropertyKey
      return CoreFieldType.doubleType;
    case 304: // JoystickBase.posYPropertyKey
      return CoreFieldType.doubleType;
    case 307: // JoystickBase.originXPropertyKey
      return CoreFieldType.doubleType;
    case 308: // JoystickBase.originYPropertyKey
      return CoreFieldType.doubleType;
    case 305: // JoystickBase.widthPropertyKey
      return CoreFieldType.doubleType;
    case 306: // JoystickBase.heightPropertyKey
      return CoreFieldType.doubleType;
    case 89: // BoneBase.lengthPropertyKey
      return CoreFieldType.doubleType;
    case 90: // RootBoneBase.xPropertyKey
      return CoreFieldType.doubleType;
    case 91: // RootBoneBase.yPropertyKey
      return CoreFieldType.doubleType;
    case 104: // SkinBase.xxPropertyKey
      return CoreFieldType.doubleType;
    case 105: // SkinBase.yxPropertyKey
      return CoreFieldType.doubleType;
    case 106: // SkinBase.xyPropertyKey
      return CoreFieldType.doubleType;
    case 107: // SkinBase.yyPropertyKey
      return CoreFieldType.doubleType;
    case 108: // SkinBase.txPropertyKey
      return CoreFieldType.doubleType;
    case 109: // SkinBase.tyPropertyKey
      return CoreFieldType.doubleType;
    case 96: // TendonBase.xxPropertyKey
      return CoreFieldType.doubleType;
    case 97: // TendonBase.yxPropertyKey
      return CoreFieldType.doubleType;
    case 98: // TendonBase.xyPropertyKey
      return CoreFieldType.doubleType;
    case 99: // TendonBase.yyPropertyKey
      return CoreFieldType.doubleType;
    case 100: // TendonBase.txPropertyKey
      return CoreFieldType.doubleType;
    case 101: // TendonBase.tyPropertyKey
      return CoreFieldType.doubleType;
    case 327: // TextModifierRangeBase.modifyFromPropertyKey
      return CoreFieldType.doubleType;
    case 336: // TextModifierRangeBase.modifyToPropertyKey
      return CoreFieldType.doubleType;
    case 334: // TextModifierRangeBase.strengthPropertyKey
      return CoreFieldType.doubleType;
    case 317: // TextModifierRangeBase.falloffFromPropertyKey
      return CoreFieldType.doubleType;
    case 318: // TextModifierRangeBase.falloffToPropertyKey
      return CoreFieldType.doubleType;
    case 319: // TextModifierRangeBase.offsetPropertyKey
      return CoreFieldType.doubleType;
    case 321: // TextVariationModifierBase.axisValuePropertyKey
      return CoreFieldType.doubleType;
    case 328: // TextModifierGroupBase.originXPropertyKey
      return CoreFieldType.doubleType;
    case 329: // TextModifierGroupBase.originYPropertyKey
      return CoreFieldType.doubleType;
    case 324: // TextModifierGroupBase.opacityPropertyKey
      return CoreFieldType.doubleType;
    case 322: // TextModifierGroupBase.xPropertyKey
      return CoreFieldType.doubleType;
    case 323: // TextModifierGroupBase.yPropertyKey
      return CoreFieldType.doubleType;
    case 332: // TextModifierGroupBase.rotationPropertyKey
      return CoreFieldType.doubleType;
    case 330: // TextModifierGroupBase.scaleXPropertyKey
      return CoreFieldType.doubleType;
    case 331: // TextModifierGroupBase.scaleYPropertyKey
      return CoreFieldType.doubleType;
    case 274: // TextStyleBase.fontSizePropertyKey
      return CoreFieldType.doubleType;
    case 370: // TextStyleBase.lineHeightPropertyKey
      return CoreFieldType.doubleType;
    case 390: // TextStyleBase.letterSpacingPropertyKey
      return CoreFieldType.doubleType;
    case 288: // TextStyleAxisBase.axisValuePropertyKey
      return CoreFieldType.doubleType;
    case 285: // TextBase.widthPropertyKey
      return CoreFieldType.doubleType;
    case 286: // TextBase.heightPropertyKey
      return CoreFieldType.doubleType;
    case 366: // TextBase.originXPropertyKey
      return CoreFieldType.doubleType;
    case 367: // TextBase.originYPropertyKey
      return CoreFieldType.doubleType;
    case 371: // TextBase.paragraphSpacingPropertyKey
      return CoreFieldType.doubleType;
    case 207: // DrawableAssetBase.heightPropertyKey
      return CoreFieldType.doubleType;
    case 208: // DrawableAssetBase.widthPropertyKey
      return CoreFieldType.doubleType;
    case 188: // TransformComponentConstraintBase.offsetPropertyKey
      return CoreFieldType.boolType;
    case 189: // TransformComponentConstraintBase.doesCopyPropertyKey
      return CoreFieldType.boolType;
    case 190: // TransformComponentConstraintBase.minPropertyKey
      return CoreFieldType.boolType;
    case 191: // TransformComponentConstraintBase.maxPropertyKey
      return CoreFieldType.boolType;
    case 192: // TransformComponentConstraintYBase.doesCopyYPropertyKey
      return CoreFieldType.boolType;
    case 193: // TransformComponentConstraintYBase.minYPropertyKey
      return CoreFieldType.boolType;
    case 194: // TransformComponentConstraintYBase.maxYPropertyKey
      return CoreFieldType.boolType;
    case 174: // IKConstraintBase.invertDirectionPropertyKey
      return CoreFieldType.boolType;
    case 364: // FollowPathConstraintBase.orientPropertyKey
      return CoreFieldType.boolType;
    case 365: // FollowPathConstraintBase.offsetPropertyKey
      return CoreFieldType.boolType;
    case 62: // LinearAnimationBase.enableWorkAreaPropertyKey
      return CoreFieldType.boolType;
    case 376: // LinearAnimationBase.quantizePropertyKey
      return CoreFieldType.boolType;
    case 201: // NestedSimpleAnimationBase.isPlayingPropertyKey
      return CoreFieldType.boolType;
    case 181: // KeyFrameBoolBase.valuePropertyKey
      return CoreFieldType.boolType;
    case 238: // NestedBoolBase.nestedValuePropertyKey
      return CoreFieldType.boolType;
    case 141: // StateMachineBoolBase.valuePropertyKey
      return CoreFieldType.boolType;
    case 41: // ShapePaintBase.isVisiblePropertyKey
      return CoreFieldType.boolType;
    case 50: // StrokeBase.transformAffectsStrokePropertyKey
      return CoreFieldType.boolType;
    case 32: // PointsPathBase.isClosedPropertyKey
      return CoreFieldType.boolType;
    case 164: // RectangleBase.linkCornerRadiusPropertyKey
      return CoreFieldType.boolType;
    case 94: // ClippingShapeBase.isVisiblePropertyKey
      return CoreFieldType.boolType;
    case 245: // CustomPropertyBooleanBase.propertyValuePropertyKey
      return CoreFieldType.boolType;
    case 196: // ArtboardBase.clipPropertyKey
      return CoreFieldType.boolType;
    case 333: // TextModifierRangeBase.clampPropertyKey
      return CoreFieldType.boolType;
    case 88: // KeyFrameColorBase.valuePropertyKey
      return CoreFieldType.colorType;
    case 37: // SolidColorBase.colorValuePropertyKey
      return CoreFieldType.colorType;
    case 38: // GradientStopBase.colorValuePropertyKey
      return CoreFieldType.colorType;
    case 223: // MeshBase.triangleIndexBytesPropertyKey
      return CoreFieldType.bytesType;
    case 359: // FileAssetBase.cdnUuidPropertyKey
      return CoreFieldType.bytesType;
    case 212: // FileAssetContentsBase.bytesPropertyKey
      return CoreFieldType.bytesType;

    default:
        return CoreFieldType.nullType;
  }
}

/// convert field type to class name
String convertFieldTypeToClassName(int value) {
  switch (value) {
    case 4:
      return "ComponentBase.namePropertyKey";
    case 55:
      return "AnimationBase.namePropertyKey";
    case 138:
      return "StateMachineComponentBase.namePropertyKey";
    case 280:
      return "KeyFrameStringBase.valuePropertyKey";
    case 248:
      return "OpenUrlEventBase.urlPropertyKey";
    case 268:
      return "TextValueRunBase.textPropertyKey";
    case 246:
      return "CustomPropertyStringBase.propertyValuePropertyKey";
    case 203:
      return "AssetBase.namePropertyKey";
    case 362:
      return "FileAssetBase.cdnBaseUrlPropertyKey";
    case 5:
      return "ComponentBase.parentIdPropertyKey";
    case 119:
      return "DrawTargetBase.drawableIdPropertyKey";
    case 120:
      return "DrawTargetBase.placementValuePropertyKey";
    case 173:
      return "TargetedConstraintBase.targetIdPropertyKey";
    case 178:
      return "DistanceConstraintBase.modeValuePropertyKey";
    case 179:
      return "TransformSpaceConstraintBase.sourceSpaceValuePropertyKey";
    case 180:
      return "TransformSpaceConstraintBase.destSpaceValuePropertyKey";
    case 195:
      return "TransformComponentConstraintBase.minMaxSpaceValuePropertyKey";
    case 175:
      return "IKConstraintBase.parentBoneCountPropertyKey";
    case 23:
      return "DrawableBase.blendModeValuePropertyKey";
    case 129:
      return "DrawableBase.drawableFlagsPropertyKey";
    case 197:
      return "NestedArtboardBase.artboardIdPropertyKey";
    case 198:
      return "NestedAnimationBase.animationIdPropertyKey";
    case 296:
      return "SoloBase.activeComponentIdPropertyKey";
    case 389:
      return "ListenerFireEventBase.eventIdPropertyKey";
    case 56:
      return "LinearAnimationBase.fpsPropertyKey";
    case 57:
      return "LinearAnimationBase.durationPropertyKey";
    case 59:
      return "LinearAnimationBase.loopValuePropertyKey";
    case 60:
      return "LinearAnimationBase.workStartPropertyKey";
    case 61:
      return "LinearAnimationBase.workEndPropertyKey";
    case 227:
      return "ListenerInputChangeBase.inputIdPropertyKey";
    case 149:
      return "AnimationStateBase.animationIdPropertyKey";
    case 237:
      return "NestedInputBase.inputIdPropertyKey";
    case 51:
      return "KeyedObjectBase.objectIdPropertyKey";
    case 165:
      return "BlendAnimationBase.animationIdPropertyKey";
    case 168:
      return "BlendAnimationDirectBase.inputIdPropertyKey";
    case 298:
      return "BlendAnimationDirectBase.blendSourcePropertyKey";
    case 155:
      return "TransitionConditionBase.inputIdPropertyKey";
    case 53:
      return "KeyedPropertyBase.propertyKeyPropertyKey";
    case 224:
      return "StateMachineListenerBase.targetIdPropertyKey";
    case 225:
      return "StateMachineListenerBase.listenerTypeValuePropertyKey";
    case 67:
      return "KeyFrameBase.framePropertyKey";
    case 68:
      return "KeyFrameBase.interpolationTypePropertyKey";
    case 69:
      return "KeyFrameBase.interpolatorIdPropertyKey";
    case 122:
      return "KeyFrameIdBase.valuePropertyKey";
    case 228:
      return "ListenerBoolChangeBase.valuePropertyKey";
    case 240:
      return "ListenerAlignTargetBase.targetIdPropertyKey";
    case 156:
      return "TransitionValueConditionBase.opValuePropertyKey";
    case 151:
      return "StateTransitionBase.stateToIdPropertyKey";
    case 152:
      return "StateTransitionBase.flagsPropertyKey";
    case 158:
      return "StateTransitionBase.durationPropertyKey";
    case 160:
      return "StateTransitionBase.exitTimePropertyKey";
    case 349:
      return "StateTransitionBase.interpolationTypePropertyKey";
    case 350:
      return "StateTransitionBase.interpolatorIdPropertyKey";
    case 392:
      return "StateMachineFireEventBase.eventIdPropertyKey";
    case 393:
      return "StateMachineFireEventBase.occursValuePropertyKey";
    case 167:
      return "BlendState1DBase.inputIdPropertyKey";
    case 171:
      return "BlendStateTransitionBase.exitBlendAnimationIdPropertyKey";
    case 48:
      return "StrokeBase.capPropertyKey";
    case 49:
      return "StrokeBase.joinPropertyKey";
    case 117:
      return "TrimPathBase.modeValuePropertyKey";
    case 40:
      return "FillBase.fillRulePropertyKey";
    case 128:
      return "PathBase.pathFlagsPropertyKey";
    case 102:
      return "WeightBase.valuesPropertyKey";
    case 103:
      return "WeightBase.indicesPropertyKey";
    case 110:
      return "CubicWeightBase.inValuesPropertyKey";
    case 111:
      return "CubicWeightBase.inIndicesPropertyKey";
    case 112:
      return "CubicWeightBase.outValuesPropertyKey";
    case 113:
      return "CubicWeightBase.outIndicesPropertyKey";
    case 92:
      return "ClippingShapeBase.sourceIdPropertyKey";
    case 93:
      return "ClippingShapeBase.fillRulePropertyKey";
    case 125:
      return "PolygonBase.pointsPropertyKey";
    case 206:
      return "ImageBase.assetIdPropertyKey";
    case 121:
      return "DrawRulesBase.drawTargetIdPropertyKey";
    case 236:
      return "ArtboardBase.defaultStateMachineIdPropertyKey";
    case 301:
      return "JoystickBase.xIdPropertyKey";
    case 302:
      return "JoystickBase.yIdPropertyKey";
    case 312:
      return "JoystickBase.joystickFlagsPropertyKey";
    case 313:
      return "JoystickBase.handleSourceIdPropertyKey";
    case 249:
      return "OpenUrlEventBase.targetValuePropertyKey";
    case 95:
      return "TendonBase.boneIdPropertyKey";
    case 316:
      return "TextModifierRangeBase.unitsValuePropertyKey";
    case 325:
      return "TextModifierRangeBase.typeValuePropertyKey";
    case 326:
      return "TextModifierRangeBase.modeValuePropertyKey";
    case 378:
      return "TextModifierRangeBase.runIdPropertyKey";
    case 356:
      return "TextStyleFeatureBase.tagPropertyKey";
    case 357:
      return "TextStyleFeatureBase.featureValuePropertyKey";
    case 320:
      return "TextVariationModifierBase.axisTagPropertyKey";
    case 335:
      return "TextModifierGroupBase.modifierFlagsPropertyKey";
    case 279:
      return "TextStyleBase.fontAssetIdPropertyKey";
    case 289:
      return "TextStyleAxisBase.tagPropertyKey";
    case 281:
      return "TextBase.alignValuePropertyKey";
    case 284:
      return "TextBase.sizingValuePropertyKey";
    case 287:
      return "TextBase.overflowValuePropertyKey";
    case 377:
      return "TextBase.originValuePropertyKey";
    case 272:
      return "TextValueRunBase.styleIdPropertyKey";
    case 204:
      return "FileAssetBase.assetIdPropertyKey";
    case 243:
      return "CustomPropertyNumberBase.propertyValuePropertyKey";
    case 172:
      return "ConstraintBase.strengthPropertyKey";
    case 177:
      return "DistanceConstraintBase.distancePropertyKey";
    case 182:
      return "TransformComponentConstraintBase.copyFactorPropertyKey";
    case 183:
      return "TransformComponentConstraintBase.minValuePropertyKey";
    case 184:
      return "TransformComponentConstraintBase.maxValuePropertyKey";
    case 185:
      return "TransformComponentConstraintYBase.copyFactorYPropertyKey";
    case 186:
      return "TransformComponentConstraintYBase.minValueYPropertyKey";
    case 187:
      return "TransformComponentConstraintYBase.maxValueYPropertyKey";
    case 363:
      return "FollowPathConstraintBase.distancePropertyKey";
    case 372:
      return "TransformConstraintBase.originXPropertyKey";
    case 373:
      return "TransformConstraintBase.originYPropertyKey";
    case 18:
      return "WorldTransformComponentBase.opacityPropertyKey";
    case 15:
      return "TransformComponentBase.rotationPropertyKey";
    case 16:
      return "TransformComponentBase.scaleXPropertyKey";
    case 17:
      return "TransformComponentBase.scaleYPropertyKey";
    case 13:
      return "NodeBase.xPropertyKey";
    case 14:
      return "NodeBase.yPropertyKey";
    case 58:
      return "LinearAnimationBase.speedPropertyKey";
    case 200:
      return "NestedLinearAnimationBase.mixPropertyKey";
    case 199:
      return "NestedSimpleAnimationBase.speedPropertyKey";
    case 292:
      return "AdvanceableStateBase.speedPropertyKey";
    case 297:
      return "BlendAnimationDirectBase.mixValuePropertyKey";
    case 140:
      return "StateMachineNumberBase.valuePropertyKey";
    case 63:
      return "CubicInterpolatorBase.x1PropertyKey";
    case 64:
      return "CubicInterpolatorBase.y1PropertyKey";
    case 65:
      return "CubicInterpolatorBase.x2PropertyKey";
    case 66:
      return "CubicInterpolatorBase.y2PropertyKey";
    case 157:
      return "TransitionNumberConditionBase.valuePropertyKey";
    case 337:
      return "CubicInterpolatorComponentBase.x1PropertyKey";
    case 338:
      return "CubicInterpolatorComponentBase.y1PropertyKey";
    case 339:
      return "CubicInterpolatorComponentBase.x2PropertyKey";
    case 340:
      return "CubicInterpolatorComponentBase.y2PropertyKey";
    case 229:
      return "ListenerNumberChangeBase.valuePropertyKey";
    case 70:
      return "KeyFrameDoubleBase.valuePropertyKey";
    case 239:
      return "NestedNumberBase.nestedValuePropertyKey";
    case 166:
      return "BlendAnimation1DBase.valuePropertyKey";
    case 202:
      return "NestedRemapAnimationBase.timePropertyKey";
    case 42:
      return "LinearGradientBase.startXPropertyKey";
    case 33:
      return "LinearGradientBase.startYPropertyKey";
    case 34:
      return "LinearGradientBase.endXPropertyKey";
    case 35:
      return "LinearGradientBase.endYPropertyKey";
    case 46:
      return "LinearGradientBase.opacityPropertyKey";
    case 47:
      return "StrokeBase.thicknessPropertyKey";
    case 39:
      return "GradientStopBase.positionPropertyKey";
    case 114:
      return "TrimPathBase.startPropertyKey";
    case 115:
      return "TrimPathBase.endPropertyKey";
    case 116:
      return "TrimPathBase.offsetPropertyKey";
    case 24:
      return "VertexBase.xPropertyKey";
    case 25:
      return "VertexBase.yPropertyKey";
    case 215:
      return "MeshVertexBase.uPropertyKey";
    case 216:
      return "MeshVertexBase.vPropertyKey";
    case 26:
      return "StraightVertexBase.radiusPropertyKey";
    case 79:
      return "CubicAsymmetricVertexBase.rotationPropertyKey";
    case 80:
      return "CubicAsymmetricVertexBase.inDistancePropertyKey";
    case 81:
      return "CubicAsymmetricVertexBase.outDistancePropertyKey";
    case 20:
      return "ParametricPathBase.widthPropertyKey";
    case 21:
      return "ParametricPathBase.heightPropertyKey";
    case 123:
      return "ParametricPathBase.originXPropertyKey";
    case 124:
      return "ParametricPathBase.originYPropertyKey";
    case 31:
      return "RectangleBase.cornerRadiusTLPropertyKey";
    case 161:
      return "RectangleBase.cornerRadiusTRPropertyKey";
    case 162:
      return "RectangleBase.cornerRadiusBLPropertyKey";
    case 163:
      return "RectangleBase.cornerRadiusBRPropertyKey";
    case 82:
      return "CubicMirroredVertexBase.rotationPropertyKey";
    case 83:
      return "CubicMirroredVertexBase.distancePropertyKey";
    case 126:
      return "PolygonBase.cornerRadiusPropertyKey";
    case 127:
      return "StarBase.innerRadiusPropertyKey";
    case 380:
      return "ImageBase.originXPropertyKey";
    case 381:
      return "ImageBase.originYPropertyKey";
    case 84:
      return "CubicDetachedVertexBase.inRotationPropertyKey";
    case 85:
      return "CubicDetachedVertexBase.inDistancePropertyKey";
    case 86:
      return "CubicDetachedVertexBase.outRotationPropertyKey";
    case 87:
      return "CubicDetachedVertexBase.outDistancePropertyKey";
    case 7:
      return "ArtboardBase.widthPropertyKey";
    case 8:
      return "ArtboardBase.heightPropertyKey";
    case 9:
      return "ArtboardBase.xPropertyKey";
    case 10:
      return "ArtboardBase.yPropertyKey";
    case 11:
      return "ArtboardBase.originXPropertyKey";
    case 12:
      return "ArtboardBase.originYPropertyKey";
    case 299:
      return "JoystickBase.xPropertyKey";
    case 300:
      return "JoystickBase.yPropertyKey";
    case 303:
      return "JoystickBase.posXPropertyKey";
    case 304:
      return "JoystickBase.posYPropertyKey";
    case 307:
      return "JoystickBase.originXPropertyKey";
    case 308:
      return "JoystickBase.originYPropertyKey";
    case 305:
      return "JoystickBase.widthPropertyKey";
    case 306:
      return "JoystickBase.heightPropertyKey";
    case 89:
      return "BoneBase.lengthPropertyKey";
    case 90:
      return "RootBoneBase.xPropertyKey";
    case 91:
      return "RootBoneBase.yPropertyKey";
    case 104:
      return "SkinBase.xxPropertyKey";
    case 105:
      return "SkinBase.yxPropertyKey";
    case 106:
      return "SkinBase.xyPropertyKey";
    case 107:
      return "SkinBase.yyPropertyKey";
    case 108:
      return "SkinBase.txPropertyKey";
    case 109:
      return "SkinBase.tyPropertyKey";
    case 96:
      return "TendonBase.xxPropertyKey";
    case 97:
      return "TendonBase.yxPropertyKey";
    case 98:
      return "TendonBase.xyPropertyKey";
    case 99:
      return "TendonBase.yyPropertyKey";
    case 100:
      return "TendonBase.txPropertyKey";
    case 101:
      return "TendonBase.tyPropertyKey";
    case 327:
      return "TextModifierRangeBase.modifyFromPropertyKey";
    case 336:
      return "TextModifierRangeBase.modifyToPropertyKey";
    case 334:
      return "TextModifierRangeBase.strengthPropertyKey";
    case 317:
      return "TextModifierRangeBase.falloffFromPropertyKey";
    case 318:
      return "TextModifierRangeBase.falloffToPropertyKey";
    case 319:
      return "TextModifierRangeBase.offsetPropertyKey";
    case 321:
      return "TextVariationModifierBase.axisValuePropertyKey";
    case 328:
      return "TextModifierGroupBase.originXPropertyKey";
    case 329:
      return "TextModifierGroupBase.originYPropertyKey";
    case 324:
      return "TextModifierGroupBase.opacityPropertyKey";
    case 322:
      return "TextModifierGroupBase.xPropertyKey";
    case 323:
      return "TextModifierGroupBase.yPropertyKey";
    case 332:
      return "TextModifierGroupBase.rotationPropertyKey";
    case 330:
      return "TextModifierGroupBase.scaleXPropertyKey";
    case 331:
      return "TextModifierGroupBase.scaleYPropertyKey";
    case 274:
      return "TextStyleBase.fontSizePropertyKey";
    case 370:
      return "TextStyleBase.lineHeightPropertyKey";
    case 390:
      return "TextStyleBase.letterSpacingPropertyKey";
    case 288:
      return "TextStyleAxisBase.axisValuePropertyKey";
    case 285:
      return "TextBase.widthPropertyKey";
    case 286:
      return "TextBase.heightPropertyKey";
    case 366:
      return "TextBase.originXPropertyKey";
    case 367:
      return "TextBase.originYPropertyKey";
    case 371:
      return "TextBase.paragraphSpacingPropertyKey";
    case 207:
      return "DrawableAssetBase.heightPropertyKey";
    case 208:
      return "DrawableAssetBase.widthPropertyKey";
    case 188:
      return "TransformComponentConstraintBase.offsetPropertyKey";
    case 189:
      return "TransformComponentConstraintBase.doesCopyPropertyKey";
    case 190:
      return "TransformComponentConstraintBase.minPropertyKey";
    case 191:
      return "TransformComponentConstraintBase.maxPropertyKey";
    case 192:
      return "TransformComponentConstraintYBase.doesCopyYPropertyKey";
    case 193:
      return "TransformComponentConstraintYBase.minYPropertyKey";
    case 194:
      return "TransformComponentConstraintYBase.maxYPropertyKey";
    case 174:
      return "IKConstraintBase.invertDirectionPropertyKey";
    case 364:
      return "FollowPathConstraintBase.orientPropertyKey";
    case 365:
      return "FollowPathConstraintBase.offsetPropertyKey";
    case 62:
      return "LinearAnimationBase.enableWorkAreaPropertyKey";
    case 376:
      return "LinearAnimationBase.quantizePropertyKey";
    case 201:
      return "NestedSimpleAnimationBase.isPlayingPropertyKey";
    case 181:
      return "KeyFrameBoolBase.valuePropertyKey";
    case 238:
      return "NestedBoolBase.nestedValuePropertyKey";
    case 141:
      return "StateMachineBoolBase.valuePropertyKey";
    case 41:
      return "ShapePaintBase.isVisiblePropertyKey";
    case 50:
      return "StrokeBase.transformAffectsStrokePropertyKey";
    case 32:
      return "PointsPathBase.isClosedPropertyKey";
    case 164:
      return "RectangleBase.linkCornerRadiusPropertyKey";
    case 94:
      return "ClippingShapeBase.isVisiblePropertyKey";
    case 245:
      return "CustomPropertyBooleanBase.propertyValuePropertyKey";
    case 196:
      return "ArtboardBase.clipPropertyKey";
    case 333:
      return "TextModifierRangeBase.clampPropertyKey";
    case 88:
      return "KeyFrameColorBase.valuePropertyKey";
    case 37:
      return "SolidColorBase.colorValuePropertyKey";
    case 38:
      return "GradientStopBase.colorValuePropertyKey";
    case 223:
      return "MeshBase.triangleIndexBytesPropertyKey";
    case 359:
      return "FileAssetBase.cdnUuidPropertyKey";
    case 212:
      return "FileAssetContentsBase.bytesPropertyKey";

    default:
        return "unknown";
  }
}
