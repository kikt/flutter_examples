import 'dart:typed_data';

import 'package:dext/dext.dart';
import 'package:tool/rive/rive_header.dart';
import 'package:tool/rive/rive_obj.dart';

class Rive {
  final Uint8List bytes;

  Rive(this.bytes) {
    header = RiveHeader(bytes);
    final pos = header.init();
    _decodeObjects(pos);
  }

  late RiveHeader header;

  List<RiveObj> objList = [];

  void _decodeObjects(int startPos) {
    var pos = startPos;
    while (pos < bytes.length) {
      final obj = RiveObj(bytes, pos);
      pos += obj.init();
      objList.add(obj);
    }
  }

  String debugString() {
    final header = this.header.debugString();
    final objList =
        this.objList.mapIndexed((index, e) => e.debugString(index)).join('\n');
    return '$header\n$objList';
  }
}
