import 'dart:typed_data';

extension BufferReaderExt on Uint8List {
  Uint8List getBytes(int start, int length) {
    return Uint8List.fromList(sublist(start, start + length));
  }

  Uleb128 readUleb128(int start) {
    int value = 0;
    int shift = 0;
    int index = start;
    int byte;
    do {
      byte = this[index];
      value |= ((byte & 0x7f) << shift);
      shift += 7;
      index++;
    } while (byte >= 0x80);

    return Uleb128(value, index - start);
  }

  int readUint32(int start) {
    return (this[start] << 24) |
        (this[start + 1] << 16) |
        (this[start + 2] << 8) |
        this[start + 3];
  }
}

class Uleb128 {
  final int value;
  final int length;

  Uleb128(this.value, this.length);
}
