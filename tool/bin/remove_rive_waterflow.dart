import 'dart:io';

import 'package:tool/rive/rive.dart';

void main() {
  final bytes = File('../assets/rive/have_waterflow.riv').readAsBytesSync();
  Rive rive = Rive(bytes);

  print(rive.debugString());
}
