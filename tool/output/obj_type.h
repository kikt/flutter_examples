// The file is generated by tool/bin/make_rive_core_type_int.dart
// Do not modify manually.

#ifndef OBJ_TYPE
#define OBJ_TYPE

#include <headers/uleb128.h>

// Read objects
string read_obj_type_key(uleb128 &v){
    local uint value = uleb128_value(v);
    switch (value)
    {
    case 1:
      return "Artboard";
    case 2:
      return "Node";
    case 3:
      return "Shape";
    case 4:
      return "Ellipse";
    case 5:
      return "StraightVertex";
    case 6:
      return "CubicDetachedVertex";
    case 7:
      return "Rectangle";
    case 8:
      return "Triangle";
    case 16:
      return "PointsPath";
    case 17:
      return "RadialGradient";
    case 18:
      return "SolidColor";
    case 19:
      return "GradientStop";
    case 20:
      return "Fill";
    case 22:
      return "LinearGradient";
    case 23:
      return "Backboard";
    case 24:
      return "Stroke";
    case 25:
      return "KeyedObject";
    case 26:
      return "KeyedProperty";
    case 27:
      return "Animation";
    case 28:
      return "CubicEaseInterpolator";
    case 30:
      return "KeyFrameDouble";
    case 31:
      return "LinearAnimation";
    case 34:
      return "CubicAsymmetricVertex";
    case 35:
      return "CubicMirroredVertex";
    case 37:
      return "KeyFrameColor";
    case 40:
      return "Bone";
    case 41:
      return "RootBone";
    case 42:
      return "ClippingShape";
    case 43:
      return "Skin";
    case 44:
      return "Tendon";
    case 45:
      return "Weight";
    case 46:
      return "CubicWeight";
    case 47:
      return "TrimPath";
    case 48:
      return "DrawTarget";
    case 49:
      return "DrawRules";
    case 50:
      return "KeyFrameId";
    case 51:
      return "Polygon";
    case 52:
      return "Star";
    case 53:
      return "StateMachine";
    case 56:
      return "StateMachineNumber";
    case 57:
      return "StateMachineLayer";
    case 58:
      return "StateMachineTrigger";
    case 59:
      return "StateMachineBool";
    case 61:
      return "AnimationState";
    case 62:
      return "AnyState";
    case 63:
      return "EntryState";
    case 64:
      return "ExitState";
    case 65:
      return "StateTransition";
    case 68:
      return "TransitionTriggerCondition";
    case 70:
      return "TransitionNumberCondition";
    case 71:
      return "TransitionBoolCondition";
    case 73:
      return "BlendStateDirect";
    case 75:
      return "BlendAnimation1D";
    case 76:
      return "BlendState1D";
    case 77:
      return "BlendAnimationDirect";
    case 78:
      return "BlendStateTransition";
    case 81:
      return "IKConstraint";
    case 82:
      return "DistanceConstraint";
    case 83:
      return "TransformConstraint";
    case 84:
      return "KeyFrameBool";
    case 87:
      return "TranslationConstraint";
    case 88:
      return "ScaleConstraint";
    case 89:
      return "RotationConstraint";
    case 92:
      return "NestedArtboard";
    case 95:
      return "NestedStateMachine";
    case 96:
      return "NestedSimpleAnimation";
    case 98:
      return "NestedRemapAnimation";
    case 100:
      return "Image";
    case 102:
      return "Folder";
    case 105:
      return "ImageAsset";
    case 106:
      return "FileAssetContents";
    case 108:
      return "MeshVertex";
    case 109:
      return "Mesh";
    case 111:
      return "ContourMeshVertex";
    case 114:
      return "StateMachineListener";
    case 115:
      return "ListenerTriggerChange";
    case 117:
      return "ListenerBoolChange";
    case 118:
      return "ListenerNumberChange";
    case 122:
      return "NestedTrigger";
    case 123:
      return "NestedBool";
    case 124:
      return "NestedNumber";
    case 126:
      return "ListenerAlignTarget";
    case 127:
      return "CustomPropertyNumber";
    case 128:
      return "Event";
    case 129:
      return "CustomPropertyBoolean";
    case 130:
      return "CustomPropertyString";
    case 131:
      return "OpenUrlEvent";
    case 134:
      return "Text";
    case 135:
      return "TextValueRun";
    case 137:
      return "TextStyle";
    case 138:
      return "CubicValueInterpolator";
    case 141:
      return "FontAsset";
    case 142:
      return "KeyFrameString";
    case 144:
      return "TextStyleAxis";
    case 147:
      return "Solo";
    case 148:
      return "Joystick";
    case 158:
      return "TextModifierRange";
    case 159:
      return "TextModifierGroup";
    case 162:
      return "TextVariationModifier";
    case 163:
      return "CubicInterpolatorComponent";
    case 164:
      return "TextStyleFeature";
    case 165:
      return "FollowPathConstraint";
    case 168:
      return "ListenerFireEvent";
    case 169:
      return "StateMachineFireEvent";
    default:
        return "unknown";
    }
}


#endif // OBJ_TYPE
