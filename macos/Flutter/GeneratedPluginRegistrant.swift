//
//  Generated file. Do not edit.
//

import FlutterMacOS
import Foundation

import flutter_image_compress_macos
import rive_common

func RegisterGeneratedPlugins(registry: FlutterPluginRegistry) {
  FlutterImageCompressMacosPlugin.register(with: registry.registrar(forPlugin: "FlutterImageCompressMacosPlugin"))
  RivePlugin.register(with: registry.registrar(forPlugin: "RivePlugin"))
}
